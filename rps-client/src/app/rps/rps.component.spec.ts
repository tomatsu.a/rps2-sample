import {async, ComponentFixture, TestBed} from '@angular/core/testing'

import {RpsComponent} from './rps.component'
import {GamePlayer} from './game-player'
import {RpsService} from './rps.service'
import {FormsModule} from '@angular/forms'
import {of} from 'rxjs'
import {GameResult} from './game-result'

describe('RpsComponent', () => {
  let component
  let fixture: ComponentFixture<RpsComponent>
  let rpsServiceSpyStub

  beforeEach(async(() => {
    rpsServiceSpyStub = jasmine.createSpyObj('RpsService', ['getResult'])
    rpsServiceSpyStub.getResult.and.returnValue(of(new GameResult('武藤wins')))
    TestBed.configureTestingModule({
      declarations: [RpsComponent],
      imports: [FormsModule],
      providers: [
        {provide: RpsService, useValue: rpsServiceSpyStub}
      ]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(RpsComponent)
    component = fixture.nativeElement
    fixture.detectChanges()
  })
  describe('初期表示', () => {

    it('タイトルを表示する', () => {
      expect(component.querySelector('h1').textContent).toEqual('じゃんけん')
    })

    it('p1の入力フィールド', () => {
      expect(component.innerText).toContain('p1')
      expect(component.querySelector('label[for=p1-name]').textContent).toEqual('名前')
      expect(component.querySelector('input[type=text]#p1-name')).toBeTruthy()
      expect(component.querySelector('label[for=p1-throw-hand]').textContent).toEqual('出し手')
      expect(component.querySelector('input[type=text]#p1-throw-hand')).toBeTruthy()
    })

    it('p2の入力フィールド', () => {
      expect(component.innerText).toContain('p2')
      expect(component.querySelector('label[for=p2-name]').textContent).toEqual('名前')
      expect(component.querySelector('input[type=text]#p2-name')).toBeTruthy()
      expect(component.querySelector('label[for=p2-throw-hand]').textContent).toEqual('出し手')
      expect(component.querySelector('input[type=text]#p2-throw-hand')).toBeTruthy()
    })

    it('playボタン', () => {
      expect(component.querySelector('button').textContent).toEqual('play')
    })

    it('勝負結果が表示されない', () => {
      expect(component.querySelector('div.result')).toBeFalsy()
    })
  })

  describe('playボタンを押下する', () => {
    beforeEach(() => {
      component.querySelector('input[type=text]#p1-name').value = 'さとう'
      component.querySelector('input[type=text]#p1-name').dispatchEvent(new Event('input'))
      component.querySelector('input[type=text]#p1-throw-hand').value = 'SCISSORS'
      component.querySelector('input[type=text]#p1-throw-hand').dispatchEvent(new Event('input'))
      component.querySelector('input[type=text]#p2-name').value = '武藤'
      component.querySelector('input[type=text]#p2-name').dispatchEvent(new Event('input'))
      component.querySelector('input[type=text]#p2-throw-hand').value = 'PAPER'
      component.querySelector('input[type=text]#p2-throw-hand').dispatchEvent(new Event('input'))


      component.querySelector('button').click()
      fixture.detectChanges()
    })

    it('サービスが呼ばれる', () => {
      expect(rpsServiceSpyStub.getResult).toHaveBeenCalledWith(
        new GamePlayer('さとう', 'SCISSORS'),
        new GamePlayer('武藤', 'PAPER')
      )
    })

    it('勝敗の結果を表示する', () => {
      expect(component.querySelector('div.result').textContent).toEqual('武藤wins')
    })
  })
})
