import {RpsService} from './rps.service'
import {HttpClientService} from '../http-client.service'
import {of} from 'rxjs'
import {GamePlayer} from './game-player'
import SpyObj = jasmine.SpyObj

describe('RpsService', () => {
  let httpClientServiceSpyStub: SpyObj<HttpClientService>
  let gamePlayer1: GamePlayer
  let gamePlayer2: GamePlayer
  beforeEach(() => {
    httpClientServiceSpyStub = jasmine.createSpyObj('HttpClientService', ['get'])
    gamePlayer1 = new GamePlayer('さとう', 'SCISSORS')
    gamePlayer2 = new GamePlayer('武藤', 'PAPER')
  })

  describe('get', () => {
    it('HTTPエンドポイントをたたく', () => {
      httpClientServiceSpyStub.get.and.returnValue(of({result: '武藤wins'}))
      const rpsSrivice = new RpsService(httpClientServiceSpyStub)

      rpsSrivice.getResult(gamePlayer1, gamePlayer2)

      expect(httpClientServiceSpyStub.get).toHaveBeenCalledWith('/rps', gamePlayer1, gamePlayer2)
    })

    it('HttpServiceから取得されたbodyを取り出して返す', () => {
      httpClientServiceSpyStub.get.and.returnValue(of({result: '武藤wins'}))

      const rpsSrivice = new RpsService(httpClientServiceSpyStub)

      expect(rpsSrivice.getResult(gamePlayer1, gamePlayer2).subscribe(value => {
          expect(value).toEqual({result: '武藤wins'})
        })
      )
    })
  })
})


