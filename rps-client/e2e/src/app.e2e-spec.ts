import {AppPage} from './app.po'
import {browser, logging} from 'protractor'

describe('workspace-project App', () => {
  let page: AppPage

  beforeEach(() => {
    page = new AppPage()
  })

  it('画面が表示される', () => {
    page.navigateTo()
    expect(page.getTitleText()).toEqual('じゃんけん')
  })

  it('playボタンを押下すると勝負結果が表示される', () => {
    page.playerNameInput(1, 'さとう')
    AppPage.playerThrowHandInput(1, 'SCISSORS')
    page.playerNameInput(2, '武藤')
    AppPage.playerThrowHandInput(2, 'ROCK')
    page.clickPlayButton()

    expect(page.getGameResultText()).toEqual('武藤wins')

  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry))
  })
})
