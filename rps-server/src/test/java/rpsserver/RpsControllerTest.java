package rpsserver;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class RpsControllerTest {
    MultiValueMap<String, String> params;
    MockMvc rpsController;

    @Mock
    RpsService rpsService;

    @Before
    public void setUp() throws Exception {
        initMocks(this);
        rpsController = standaloneSetup(new RpsController(rpsService)).build();
        params = new LinkedMultiValueMap<>();
        params.add("p1Name", "さとう");
        params.add("p1ThrowHand", "SCISSORS");
        params.add("p2Name", "武藤");
        params.add("p2ThrowHand", "PAPER");
    }

    @Test
    public void getResult_asksService() throws Exception {

        rpsController.perform(get("/api/rps")
                .params(params));

        Mockito.verify(rpsService).playRps(
                new GamePlayer("さとう", Throw.SCISSORS.toString()),
                new GamePlayer("武藤", Throw.PAPER.toString())
        );
    }

    @Test
    public void getResult_returnData() throws Exception {
        when(rpsService.playRps(any(), any())).thenReturn("さとうwins");

        rpsController.perform(get("/api/rps")
                .params(params))
                .andExpect(jsonPath("$.result", equalTo("さとうwins")));
    }
}