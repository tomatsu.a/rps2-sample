package rpsserver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class RpsServiceTest {
    private RpsService rpsService;

    @Before
    public void setUp() {
        rpsService = new RpsService();
    }

    @Test
    public void rockVsScissors() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.ROCK.toString()),
                new GamePlayer("加藤", Throw.SCISSORS.toString())
        );

        assertEquals("さとうwins", result);
    }

    @Test
    public void rockVsPaper() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.ROCK.toString()),
                new GamePlayer("加藤", Throw.PAPER.toString())
        );
        assertEquals("加藤wins", result);
    }

    @Test
    public void rockVsRock() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.ROCK.toString()),
                new GamePlayer("加藤", Throw.ROCK.toString())
        );
        assertEquals("tie", result);
    }

    @Test
    public void scissorsVsRock() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.SCISSORS.toString()),
                new GamePlayer("加藤", Throw.ROCK.toString())
        );
        assertEquals("加藤wins", result);
    }

    @Test
    public void scissorsVsScissors() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.SCISSORS.toString()),
                new GamePlayer("加藤", Throw.SCISSORS.toString())
        );
        assertEquals("tie", result);
    }

    @Test
    public void scissorsVsPaper() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.SCISSORS.toString()),
                new GamePlayer("加藤", Throw.PAPER.toString())
        );
        assertEquals("さとうwins", result);
    }

    @Test
    public void paperVsRock() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.PAPER.toString()),
                new GamePlayer("加藤", Throw.ROCK.toString())
        );
        assertEquals("さとうwins", result);
    }

    @Test
    public void paperVsScissors() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.PAPER.toString()),
                new GamePlayer("加藤", Throw.SCISSORS.toString())
        );
        assertEquals("加藤wins", result);
    }

    @Test
    public void paperVsPaper() {
        String result = rpsService.playRps(
                new GamePlayer("さとう", Throw.PAPER.toString()),
                new GamePlayer("加藤", Throw.PAPER.toString())
        );
        assertEquals("tie", result);
    }
}