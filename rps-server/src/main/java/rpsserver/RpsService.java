package rpsserver;

import org.springframework.stereotype.Service;

@Service
public class RpsService {
    public String playRps(GamePlayer p1, GamePlayer p2) {
        if (p1.throwHand.equals(p2.throwHand)) {
            return "tie";
        }

        if (
                (p1.throwHand.equals(Throw.ROCK.toString()) && p2.throwHand.equals(Throw.PAPER.toString()))
                        || (p1.throwHand.equals(Throw.SCISSORS.toString()) && p2.throwHand.equals(Throw.ROCK.toString()))
                        || (p1.throwHand.equals(Throw.PAPER.toString()) && p2.throwHand.equals(Throw.SCISSORS.toString()))
        ) {
            return p2.name + "wins";
        } else {
            return p1.name + "wins";
        }
    }
}
