package rpsserver;

import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/rps")
public class RpsController {
    private RpsService rpsService;

    public RpsController(RpsService rpsService) {
        this.rpsService = rpsService;
    }

    @GetMapping
    public GameResult getResult(@RequestParam("p1Name") String p1Name,
                                @RequestParam("p1ThrowHand") String p1ThrowHand,
                                @RequestParam("p2Name") String p2Name,
                                @RequestParam("p2ThrowHand") String p2ThrowHand) {
        return new GameResult(
                rpsService.playRps(
                        new GamePlayer(p1Name, p1ThrowHand),
                        new GamePlayer(p2Name, p2ThrowHand)
                )
        );
    }
}
